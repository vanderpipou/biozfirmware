/******************************************************************************/
/*	Name:	LTC6904.h						      */
/*	Author: Steve Mermet						      */
/*	Date:	11.10.2019						      */
/*									      */
/******************************************************************************/

#ifndef LTC6904_H
#define LTC6904_H

#include <stdint.h>

// Define if pin ADR is High or Low
#define ADR_PIN		0

#if !ADRPIN
#define LTC6904_ADDR	0b0010111
#else
#define LTC6904_ADDR	0b0010110
#endif

#define OCT_DAC_TO_MSB(oct, dac) (uint8_t)(((oct&0x0F)<<4)|((dac&0x03C0)>>6))
#define DEC_CFN_TO_MSB(dac, cfn) (uint8_t)(((dac&0x3F)<<2)|(cfn))

#endif//LTC6904_H
