/********************************************************************************/
/*	Name:	AD5933.h						                                    */
/*	Author: Steve Mermet						                                */
/*	Date:	11.10.2019						                                    */
/*									                                            */
/********************************************************************************/

#ifndef AD5933_H
#define AD5933_H

#include <stdint.h>
#include "nrf_twi_mngr.h"

#ifdef __cplusplus
extern "C" {
#endif

#define AD5933_ADDR	0b0001101

/**
 * @brief AD5933 register
 */
typedef struct {
    uint16_t	ctrl_reg;   	///< Control register R/W
    uint32_t	start_freq_reg;	///< StratFrquency Register R/W
    uint32_t	freq_inc_reg;	///< Frequency Increment Register R/W
    uint16_t	nb_inc_reg;	///< Number of Increments Register R/W
    uint16_t	nb_set_reg;	///< Number of Settling Times Cycles Register R/W
    uint8_t	status_reg;	///< Status Register RO
    uint16_t	temp_reg;	///< Temperature Register RO
    uint16_t	real_reg;	///< Real Data Register RO
    uint16_t	imag_reg;	///< Imaginary Data Register RO
} ad5933_register_t;

#define CTRL_REG_OFFSET		0x80
#define START_REG_OFFSET	0x82
#define FREQ_INC_REG_OFFSET	0x85
#define NB_INC_REG_OFFSET	0x88
#define NB_SET_REG_OFFSET	0x8A
#define STATUS_REG_OFFSET	0x8F
#define TEMP_REG_OFFSET		0x92
#define REAL_REG_OFFSET		0x94
#define IAMG_REG_OFFSET		0x96

#define BLOCK_WRITE_CMD		0xA0
#define BLOCK_READ_CMD		0xA1
#define ADDR_POINTER_CMD	0xB0


#ifdef __cplusplus
}
#endif

#endif //AD5933_H
