/********************************************************************************/
/*	Name:	AD5933.h						                                    */
/*	Author: Steve Mermet						                                */
/*	Date:	11.10.2019						                                    */
/*									                                            */
/********************************************************************************/

#ifndef AD5933_H
#define AD5933_H

#include <stdint.h>
#include "nrf_twi_mngr.h"

#ifdef __cplusplus
extern "C" {
#endif

#define AD5933_ADDR	0b0001101

/**
 * @brief AD5933 register
 */
typedef struct {
    uint16_t	ctrl_reg;   	///< Control register R/W
    uint32_t	start_freq_reg;	///< StratFrquency Register R/W
    uint32_t	freq_inc_reg;	///< Frequency Increment Register R/W
    uint16_t	nb_inc_reg;	///< Number of Increments Register R/W
    uint16_t	nb_set_reg;	///< Number of Settling Times Cycles Register R/W
    uint8_t	status_reg;	///< Status Register RO
    uint16_t	temp_reg;	///< Temperature Register RO
    uint16_t	real_reg;	///< Real Data Register RO
    uint16_t	imag_reg;	///< Imaginary Data Register RO
} ad5933_register_t;

typedef struct {
  nrf_twi_mngr_t const *	p_twi_mngr;	///< Pointer to the TWI Manager
  nrf_drv_twi_config_t const *	p_twi_config;	///< Pointer to the TWI Configuration
} ad5933_mngr_cfg_t;

#define CTRL_REG_OFFSET		0x80
#define START_REG_OFFSET	0x82
#define FREQ_INC_REG_OFFSET	0x85
#define NB_INC_REG_OFFSET	0x88
#define NB_SET_REG_OFFSET	0x8A
#define STATUS_REG_OFFSET	0x8F
#define TEMP_REG_OFFSET		0x92
#define REAL_REG_OFFSET		0x94
#define IMAG_REG_OFFSET		0x96

#define BLOCK_WRITE_CMD		0xA0
#define BLOCK_READ_CMD		0xA1
#define ADDR_POINTER_CMD	0xB0

// Status
#define TEMP_MEAS_VALID		0x01
#define IMP_MEAS_VALID		0x02
#define FREQ_SWEEP_COMPLETE	0x04

// Configuration defines
#define USE_EXT_CLOCK		(0x01<<3)
#define SOFT_RESET		(0x01<<4)
#define PGA_GAIN_1		(0x01)
#define PGA_GAIN_5		(0x00)

// Modes
#define MODE_SHIFT		4
#define INIT_START_FREQ		(0x01)
#define START_FREQ_SWEEP	(0x02)
#define INC_FREQ		(0x03)
#define REAPET_FREQ		(0x04)
#define MEASURE_TEMP		(0x09)
#define POWER_DOWN_MODE		(0x0A)
#define STANDBY_MODE		(0x0B)

// Output voltage
#define OUT_SHIFT		1
#define OUT_2VPP		(0x00)
#define OUT_1VPP		(0x03)
#define OUT_400MVPP		(0x02)
#define OUT_200MVPP		(0x01)

#define NB_SETTLING_CYCLES	0x0032	// Number of settling times: 50x1, see p.25
#define SETT_MULT_DEFAULT	0
#define SETT_MULT_X2		1
#define SETT_MULT_X4		3

uint32_t compute_freq_code(uint32_t, uint32_t);
uint8_t sel_range_reg(uint8_t);

void ad5933_set_pointer(uint8_t, ad5933_mngr_cfg_t);
uint8_t ad5933_read_byte(ad5933_mngr_cfg_t);
void ad5933_read_bloc(uint8_t *, uint8_t, ad5933_mngr_cfg_t);
void ad5933_write_byte(uint8_t, uint8_t, ad5933_mngr_cfg_t);
void ad5933_write_bloc(uint8_t *, uint8_t, ad5933_mngr_cfg_t);

void ad5933_set_freq(uint32_t, ad5933_mngr_cfg_t);
void ad5933_set_freq_inc(uint32_t, ad5933_mngr_cfg_t);
void ad5933_set_settling(uint16_t, uint8_t, ad5933_mngr_cfg_t);
void ad5933_set_inc(uint16_t, ad5933_mngr_cfg_t);
uint8_t ad5933_get_status(ad5933_mngr_cfg_t);
int16_t ad5933_get_data(uint8_t,ad5933_mngr_cfg_t);
void ad5933_set_mode(uint8_t, ad5933_mngr_cfg_t);
void ad5933_pga_gain(uint8_t, ad5933_mngr_cfg_t);
void ad5933_output_range(uint8_t, ad5933_mngr_cfg_t);
void ad5933_reset(ad5933_mngr_cfg_t);
void ad5933_use_ext_clk(bool, ad5933_mngr_cfg_t );


#ifdef __cplusplus
}
#endif

#endif //AD5933_H
