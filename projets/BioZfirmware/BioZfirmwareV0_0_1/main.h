#ifndef MAIN_H
#define MAIN_H

// standard includes
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// nordic sdk includes
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_bas.h"
#include "ble_dis.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_serial.h"
#include "app_sdcard.h"
#include "fds.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "bsp_btn_ble.h"
#include "sensorsim.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_twi_mngr.h"
#include "nrf_drv_clock.h"

// FreeRTOS includes
#include "nrf_sdh_freertos.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

// Mea-z-ure includes
#include "LTC6904.h"
#include "AD5933.h"

// FSM Framework
#include "fsm-framework.h"
#include "nrf_ringbuf.h"

// Boards includes
#include "custom_board.h"

// nRF Log includes
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"




#endif //MAIN_H
