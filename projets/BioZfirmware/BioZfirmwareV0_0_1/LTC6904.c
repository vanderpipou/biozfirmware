#include "LTC6904.h"

uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND write_word[2];

void ltc6904_set_freq(ltc6904_cfg_t osc_cfg){
  write_word[0] = OCT_DAC_TO_MSB(osc_cfg.oct, osc_cfg.dac);
  write_word[1] = DEC_CFN_TO_MSB(osc_cfg.dac, osc_cfg.cfn);

  static nrf_twi_mngr_transfer_t ltc6904_trsfr[] =
    {
        NRF_TWI_MNGR_WRITE(LTC6904_ADDR, &write_word, 2, 0)
    };

  // schedule transaction
    ret_code_t err_code;
    err_code = nrf_twi_mngr_perform(osc_cfg.p_twi_mngr, osc_cfg.p_twi_config,
				    ltc6904_trsfr,
  				  sizeof(ltc6904_trsfr)/sizeof(ltc6904_trsfr[0]),
  				  NULL);
    APP_ERROR_CHECK(err_code);

}

uint8_t ltc6904_oct_compute(uint32_t freq){
  if(freq>=34045952){return 15;}
  else if(freq>=17022976){return 14;}
  else if(freq>=8511488){return 13;}
  else if(freq>=4255744){return 12;}
  else if(freq>=2127872){return 11;}
  else if(freq>=1063936){return 10;}
  else if(freq>=531968){return 9;}
  else if(freq>=265984){return 8;}
  else if(freq>=132992){return 7;}
  else if(freq>=66496){return 6;}
  else if(freq>=33248){return 5;}
  else if(freq>=16624){return 4;}
  else if(freq>=8312){return 3;}
  else if(freq>=4156){return 2;}
  else if(freq>=2078){return 1;}
  else{return 0;}
}

uint16_t ltc6904_dac_compute(uint32_t freq, uint8_t oct){
  uint32_t temp, temp2, dac;

  // if oct is over 9 it temp will overflow
  // so we simply by 1024
  if(oct>=10){
      temp = 2078<<(oct);
      temp2 = temp/(freq>>10);
      dac = 2048-temp2;
      if((temp%(freq>>10))>=(freq>>11)){
	  return (uint16_t)(dac-1);
      }
      else{
	  return(uint16_t)(dac);
      }
  }
  else {
      temp = 2078<<(10+oct);
      temp2 = temp/(freq);
      dac = 2048-temp2;
      if((temp%(freq))>=(freq>>1)){
	  return (uint16_t)(dac-1);
      }
      else{
	  return(uint16_t)(dac);
      }
  }
}
