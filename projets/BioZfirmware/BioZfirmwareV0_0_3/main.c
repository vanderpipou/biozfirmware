
#include "main.h"


/************************** BLE GAP/GATT **************************************/
#define DEVICE_NAME                     "MEA-Z-ure_v4_0"                       	/**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "HEPIA"                   		/**< Manufacturer. Will be passed to Device Information Service. */
#define FW_VERSION			"BZ0.0.1"				/**< Firmaware Version. Will be passed to Device Information Service. */
#define SW_VERSION			"S132-6.1.1"				/**< Software Version. Will be passed to Device Information Service.*/
#define APP_ADV_INTERVAL                300                                     /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */

#define APP_ADV_DURATION                18000                                   /**< The advertising duration (180 seconds) in units of 10 milliseconds. */
#define APP_BLE_OBSERVER_PRIO           3                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG            1                                       /**< A tag identifying the SoftDevice BLE configuration. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                   0                                       /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                   /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                  1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                       /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                      /**< Maximum encryption key size. */

#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

//#define DEBUG
//#define TWI_SCHEDULE
//#define SEND_HELLO
//#define LOG_STATE

/************************** variable to save values ***************************/
//static uint32_t measure_save[50];
//static void init_save(void){
//  memset(&measure_save,0,sizeof(measure_save));
//}

/************************** OSC EXTERN ****************************************/
#define OSC_OCT_1HZ			6
#define OSC_OCT_100HZ			9
#define OSC_OCT_1KHZ			13

#define OSC_DAC				959

/************************** Battery Service ***********************************/
#define BATT_LVL_MEAS_INTERVAL		60000					/**< Battery level measure timer interval in ms. */

BLE_BAS_DEF(m_bas);								/**< Battery service instance. */

/************************** BLE Utility ***************************************/
NRF_BLE_GATT_DEF(m_gatt);                      		                     	/**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                             		/**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                 		/**< Advertising module instance. */

static uint16_t m_conn_handle	= BLE_CONN_HANDLE_INVALID;    			/**< Handle of the current connection. */

static ble_uuid_t m_adv_uuids[] =                                   /**< Universally unique service identifiers. */
{
    {BLE_UUID_BATTERY_SERVICE, BLE_UUID_TYPE_BLE},
    {BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}
};

/************************** Serial UART Driver ********************************/
NRF_SERIAL_UART_DEF(m_nrf_uart_id, 0);						/**< Application Serial UART config definition. */
NRF_SERIAL_DRV_UART_CONFIG_DEF(m_uart0_drv_config,
			       RX_PIN_NUMBER, TX_PIN_NUMBER,
			       RTS_PIN_NUMBER, CTS_PIN_NUMBER,
			       NRF_UART_HWFC_DISABLED,
			       NRF_UART_PARITY_EXCLUDED,
			       NRF_UART_BAUDRATE_115200,
			       UART_DEFAULT_CONFIG_IRQ_PRIORITY);		/**< Application Serial UART Driver config definition */

#define SERIAL_FIFO_TX_SIZE 32
#define SERIAL_FIFO_RX_SIZE 32

NRF_SERIAL_QUEUES_DEF(serial_queues, SERIAL_FIFO_TX_SIZE, SERIAL_FIFO_RX_SIZE);


#define SERIAL_BUFF_TX_SIZE 1
#define SERIAL_BUFF_RX_SIZE 1

NRF_SERIAL_BUFFERS_DEF(serial_buffs, SERIAL_BUFF_TX_SIZE, SERIAL_BUFF_RX_SIZE);

NRF_SERIAL_CONFIG_DEF(serial_config, NRF_SERIAL_MODE_IRQ,
                      &serial_queues, &serial_buffs, NULL, NULL);

/************************** Tasks and Timers **********************************/
#if NRF_LOG_ENABLED
static TaskHandle_t m_logger_thread;                                		/**< Definition of Logger thread. */
#endif

static TaskHandle_t m_imp_analyzer_task;

/*****************************************************************************/
/*                        SD Memory Card                                     */
/*                                                                           */
/*****************************************************************************/
/*
void sdc_handler(sdc_evt_t const * p_event){
  switch(p_event->type){
    case SDC_EVT_INIT:
      APP_ERROR_CHECK(p_event->result);
      NRF_LOG_INFO("SDC init: %d",p_event->result);
      break;
    case SDC_EVT_READ:
      APP_ERROR_CHECK(p_event->result);
      NRF_LOG_INFO("SDC read: %d",p_event->result);
      break;
    case SDC_EVT_WRITE:
      APP_ERROR_CHECK(p_event->result);
      NRF_LOG_INFO("SDC write: %d",p_event->result);
    default:break;
  }
  return;
}

uint8_t buffer[SDC_SECTOR_SIZE];

static const app_sdc_config_t sdc_cfg = {
    .mosi_pin = SPIM0_MOSI_PIN,
    .miso_pin = SPIM0_MISO_PIN,
    .sck_pin  = SPIM0_SCK_PIN,
    .cs_pin   = SPIM0_CS_PIN
};

static void sd_card_init(void){
  APP_ERROR_CHECK(app_sdc_init(&sdc_cfg, sdc_handler));
  while(app_sdc_busy_check()){}
}
*/


/*****************************************************************************/
/*                        TWI MANAGER                                        */
/*                                                                           */
/*****************************************************************************/

#define TWIM_QUEUE_SIZE 32

NRF_TWI_MNGR_DEF(m_twi_mngr, TWIM_QUEUE_SIZE, 0);				/**< Definition of the TWI Manager */

nrf_drv_twi_config_t const m_twi_config = {
      .scl		= TWIM0_SCL,
      .sda		= TWIM0_SDA,
      .frequency	= NRF_DRV_TWI_FREQ_100K,
      .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
      .clear_bus_init	= true
  };

static void twi_config(void)
{
  APP_ERROR_CHECK(nrf_twi_mngr_init(&m_twi_mngr, &m_twi_config));
}


/*****************************************************************************/
/*                       LTC6904 Configuration                               */
/*                                                                           */
/*****************************************************************************/

uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND osc_ext_reg[2];

#define CFN	0x02	// Output CLK On, Output nCLK Off

static void osc_ext_callback(ret_code_t result, void *p_user_data)
{
  //bsp_board_led_invert(1);
  if (result != NRF_SUCCESS)
  {
      NRF_LOG_WARNING("osc_ext_callback - error: %d", (int)result);
      return;
  }
  //bsp_board_led_invert(1);
  NRF_LOG_INFO("OSC EXT Configured");
  NRF_LOG_HEXDUMP_INFO(&osc_ext_reg, 2);

}

static void osc_ext_init(uint8_t oct, uint16_t dac)
{
  if(nrf_gpio_pin_read(ANAL_ON) == 0){
      return;
  }
  //fill reg
  osc_ext_reg[0] = OCT_DAC_TO_MSB(oct, dac);
  osc_ext_reg[1] = DEC_CFN_TO_MSB(dac,CFN);
  // define transfert
  static nrf_twi_mngr_transfer_t osc_ext_trsfr[] =
  {
      NRF_TWI_MNGR_WRITE(LTC6904_ADDR, &osc_ext_reg, 2, 0)
  };


  // define transaction
  static nrf_twi_mngr_transaction_t NRF_TWI_MNGR_BUFFER_LOC_IND transaction =
  {
      .callback 		= osc_ext_callback,
      .p_user_data 		= NULL,
      .p_transfers 		= osc_ext_trsfr,
      .number_of_transfers 	= sizeof(osc_ext_trsfr)/sizeof(osc_ext_trsfr[0])
  };
  //schedule transaction
  APP_ERROR_CHECK(nrf_twi_mngr_schedule(&m_twi_mngr, &transaction));
}

/*****************************************************************************/
/*                 Battery Level Measure Timer                               */
/*                                                                           */
/*****************************************************************************/
static TimerHandle_t m_battery_timer;						/**< Definition of the battery timer. */
static void  battery_level_meas_timeout_handler(void * pvParameter){
  UNUSED_PARAMETER(pvParameter);
  //NRF_LOG_INFO("hguetihuger");
  //bsp_board_led_invert(1);
  ret_code_t err_code;
  static uint8_t  battery_level;

  battery_level = (battery_level +1) %100;

  err_code = ble_bas_battery_level_update(&m_bas, battery_level, BLE_CONN_HANDLE_ALL);
  if ((err_code != NRF_SUCCESS) &&
      (err_code != NRF_ERROR_INVALID_STATE) &&
      (err_code != NRF_ERROR_RESOURCES) &&
      (err_code != NRF_ERROR_BUSY) &&
      (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
     )
  {
      APP_ERROR_HANDLER(err_code);
  }
}

/*****************************************************************************/
/*                        Measure Task                                       */
/*                                                                           */
/*****************************************************************************/
typedef enum {config, standby, init, start, sweep, decade, done} measure_state_t;	/**< States of the measurement state machine*/
typedef struct {
  bool			m_running;
  measure_state_t 	m_current_state;
  ad5933_mngr_cfg_t	m_ad5933_mngr;
  uint8_t		m_current_decade;
  uint8_t		m_current_inc;
} measure_cb_t;

#define DECADE_BEGIN	0
#define FREQ_OSC	1000000

static void imp_analyzer_task(void * v_params){
  UNUSED_PARAMETER(v_params);
  //bsp_board_led_on(1);

 /* static ad5933_mngr_cfg_t imp_ana_mngr = {
      .p_twi_mngr = &m_twi_mngr,
      .p_twi_config = &m_twi_config
  };*/

  static measure_cb_t current_meas = {
      .m_running = true,
      .m_current_state = done,
      .m_ad5933_mngr = {
	  .p_twi_mngr = &m_twi_mngr,
	  .p_twi_config = &m_twi_config
      },
      .m_current_decade = DECADE_BEGIN,
      .m_current_inc = 0
  };

  static ltc6904_cfg_t osc_ext_cfg = {
      .oct = 9,
      .dac = 959,
      .p_twi_mngr = &m_twi_mngr,
      .p_twi_config = &m_twi_config
  };

  static uint8_t meas_num = 0;

  uint8_t temp_status = 0;
  uint32_t freq_start = 1;
  uint32_t freq_osc = FREQ_OSC;

  uint32_t freq_code = 0;
  int16_t data[2] = {0,0};

  while(1){

      NRF_LOG_INFO("fruefrhef");
      if(current_meas.m_running){
	  nrf_gpio_pin_set(ANAL_ON);
	  switch(current_meas.m_current_state){
	    case config:
	      NRF_LOG_INFO("decade: %u",current_meas.m_current_decade);

	      osc_ext_cfg.oct = ltc6904_oct_compute(freq_osc);
	      osc_ext_cfg.dac = ltc6904_dac_compute(freq_osc, osc_ext_cfg.oct);

	      freq_code = compute_freq_code(freq_start,freq_osc/1000);

	      ltc6904_set_freq(osc_ext_cfg);
	      ad5933_use_ext_clk(true, current_meas.m_ad5933_mngr);
	      ad5933_pga_gain(PGA_GAIN_1, current_meas.m_ad5933_mngr);
	      ad5933_output_range(OUT_200MVPP, current_meas.m_ad5933_mngr);
	      ad5933_set_freq(freq_code,current_meas.m_ad5933_mngr);
	      ad5933_set_freq_inc(freq_code,current_meas.m_ad5933_mngr);
	      ad5933_set_settling(10,SETT_MULT_DEFAULT,current_meas.m_ad5933_mngr);
	      ad5933_set_inc(8,current_meas.m_ad5933_mngr);
	      current_meas.m_current_state = standby;
	      break;

	    case standby:
	      NRF_LOG_INFO("standby");
	      ad5933_set_mode(STANDBY_MODE, current_meas.m_ad5933_mngr);
	      current_meas.m_current_state = init;
	      break;

	    case init:
	      NRF_LOG_INFO("init");
	      ad5933_set_mode(INIT_START_FREQ, current_meas.m_ad5933_mngr);
	      vTaskDelay(2000);
	      current_meas.m_current_state = start;
	      break;

	    case start:
	      //NRF_LOG_INFO("start");
	      ad5933_set_mode(START_FREQ_SWEEP, current_meas.m_ad5933_mngr);
	      current_meas.m_current_state = sweep;
	      break;

	    case sweep:
	      NRF_LOG_INFO("sweep");
	      temp_status = ad5933_get_status(current_meas.m_ad5933_mngr);
	      if(temp_status & IMP_MEAS_VALID){
		  //NRF_LOG_INFO("measure:");
		  data[0] = ad5933_get_data(REAL_REG_OFFSET, current_meas.m_ad5933_mngr);
		  data[1] = ad5933_get_data(IMAG_REG_OFFSET, current_meas.m_ad5933_mngr);
		  meas_num = (current_meas.m_current_decade * 9) + current_meas.m_current_inc;
		  NRF_LOG_INFO("Measure n%d",meas_num);
		  NRF_LOG_INFO("real %d - imag %d",data[0],data[1]);
		  if(temp_status & FREQ_SWEEP_COMPLETE){
		      current_meas.m_current_state = done;
		      current_meas.m_current_inc = 0;
		      current_meas.m_current_decade++;
		      current_meas.m_current_decade %= 5;
		  }
		  else{
		      ad5933_set_mode(INC_FREQ, current_meas.m_ad5933_mngr);
		      current_meas.m_current_state = sweep;
		      current_meas.m_current_inc++;
		  }
	      }
	      break;

	    case decade:
	    case done:
	      //bsp_board_led_off(0);
	      freq_start = 1;
	      for(uint8_t i = 0;i<current_meas.m_current_decade;i++){
		  freq_start *= 10;
	      }
	      freq_osc = FREQ_OSC<<(current_meas.m_current_decade);
	      ad5933_set_mode(STANDBY_MODE, current_meas.m_ad5933_mngr);
	      vTaskDelay(500);
	      current_meas.m_current_state = config;
	      break;

	    default:
	      current_meas.m_current_decade = done;
	      break;
	  }
      }
      else {
	  nrf_gpio_pin_clear(ANAL_ON);
      }
      bsp_board_led_invert(1);
      vTaskDelay(500);
  }
}

/*****************************************************************************/
/*                        NRF BLE callbacks                                  */
/*                                                                           */
/*****************************************************************************/

static void sleep_mode_enter(void);

static void advertising_start(void * p_erase_bonds);

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    bool delete_bonds = false;

    pm_handler_on_pm_evt(p_evt);
    pm_handler_flash_clean(p_evt);

    switch (p_evt->evt_id)
    {
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            advertising_start(&delete_bonds);
            break;

        default:
            break;
    }
}

/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in]   p_evt   Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            NRF_LOG_INFO("Fast advertising.");
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_IDLE:
	    NRF_LOG_INFO("Sleep");
            sleep_mode_enter();
            break;

        default:
            break;
    }
}

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected");
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected");
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
static void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BSP_EVENT_WHITELIST_OFF:
            if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
            {
                err_code = ble_advertising_restart_without_whitelist(&m_advertising);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
            }
            break;

        default:
            break;
    }
}


/*****************************************************************************/
/*                        NRF BLE Init                                       */
/*                                                                           */
/*****************************************************************************/

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_UNKNOWN);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the GATT module. */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing services that will be used by the application.
 *
 * @details Initialize the Heart Rate, Battery and Device Information services.
 */
static void services_init(void)
{
    ret_code_t         err_code;
    ble_bas_init_t     bas_init;
    ble_dis_init_t     dis_init;
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    // Initialize Battery Service.
    memset(&bas_init, 0, sizeof(bas_init));

    // Here the sec level for the Battery Service can be changed/increased.
    bas_init.bl_rd_sec        = SEC_OPEN;
    bas_init.bl_cccd_wr_sec   = SEC_OPEN;
    bas_init.bl_report_rd_sec = SEC_OPEN;

    bas_init.evt_handler          = NULL;
    bas_init.support_notification = true;
    bas_init.p_report_ref         = NULL;
    bas_init.initial_batt_level   = 100;

    err_code = ble_bas_init(&m_bas, &bas_init);
    APP_ERROR_CHECK(err_code);

    // Initialize Device Information Service.
    memset(&dis_init, 0, sizeof(dis_init));

    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, (char *)MANUFACTURER_NAME);
    ble_srv_ascii_to_utf8(&dis_init.fw_rev_str, (char *)FW_VERSION);
    ble_srv_ascii_to_utf8(&dis_init.sw_rev_str, (char *)SW_VERSION);


    dis_init.dis_char_rd_sec = SEC_OPEN;

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Connection Parameters module. */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

/**@brief Function for the Peer Manager initialization. */
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}

/**@brief Clear bond information from persistent storage. */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Advertising functionality. */
static void advertising_init(void)
{
    ret_code_t             err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

/**@brief Function for starting advertising. */
static void advertising_start(void * p_erase_bonds)
{
    bool erase_bonds = *(bool*)p_erase_bonds;

    if (erase_bonds)
    {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETE_SUCCEEDED event.
    }
    else
    {
        ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
        APP_ERROR_CHECK(err_code);
    }
}

/*****************************************************************************/
/*                        Timer Configuration                                */
/*                                                                           */
/*****************************************************************************/

static void timers_init(void){
  ret_code_t err_code = app_timer_init();
  APP_ERROR_CHECK(err_code);

  // Create Timer
  m_battery_timer = xTimerCreate("BATT",
				 BATT_LVL_MEAS_INTERVAL,
				 pdTRUE,
				 NULL,
				 battery_level_meas_timeout_handler);

}

static void timers_start(void){
  UNUSED_VARIABLE(xTimerStart(m_battery_timer, 0));
}


/**@brief Function for the Serial UART initialization.
 *
 * @details Initializes the Serial URAT module.
 */
static void serial_uart_init(void)
{
  // Initialize UART module.
  ret_code_t err_code;
  err_code = nrf_serial_init(&m_nrf_uart_id, &m_uart0_drv_config, &serial_config);
  APP_ERROR_CHECK(err_code);
}


/*****************************************************************************/
/*                        Initialisations                                    */
/*                                                                           */
/*****************************************************************************/

/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    nrf_gpio_pin_clear(ANAL_ON);

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
    ret_code_t err_code;

    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

#if NRF_LOG_ENABLED
/**@brief Thread for handling the logger.
 *
 * @details This thread is responsible for processing log entries if logs are deferred.
 *          Thread flushes all log entries and suspends. It is resumed by idle task hook.
 *
 * @param[in]   arg   Pointer used for passing some arbitrary information (context) from the
 *                    osThreadCreate() call to the thread.
 */
static void logger_thread(void * arg)
{
    UNUSED_PARAMETER(arg);

    while (1)
    {
        NRF_LOG_FLUSH();

        vTaskSuspend(NULL); // Suspend myself
    }
}
#endif //NRF_LOG_ENABLED


/**@brief A function which is hooked to idle task.
 * @note Idle hook must be enabled in FreeRTOS configuration (configUSE_IDLE_HOOK).
 */
void vApplicationIdleHook( void )
{
#if NRF_LOG_ENABLED
     vTaskResume(m_logger_thread);
#endif
}

static void init_tasks(void){

#if NRF_LOG_ENABLED
  // Start execution.
  if (pdPASS != xTaskCreate(logger_thread, "LOGGER", 256, NULL, 1, &m_logger_thread))
  {
      APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
#endif
  //NRF_LOG_INFO("logger init");

  if(pdPASS != xTaskCreate(imp_analyzer_task, "IMPANALYZER",
			   configMINIMAL_STACK_SIZE+40,
			   NULL, 1, &m_imp_analyzer_task)){
      APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
  //NRF_LOG_INFO("imp analyzer init");
}


/**@brief Function for application main entry.
 */
int main(void){

  // Configure the pins NFCT P9 and P10 as GPIO otherwise the pins are used as NFCT
  if ((NRF_UICR->NFCPINS & UICR_NFCPINS_PROTECT_Msk) == (UICR_NFCPINS_PROTECT_NFC << UICR_NFCPINS_PROTECT_Pos)){
  NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
  while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
  NRF_UICR->NFCPINS &= ~UICR_NFCPINS_PROTECT_Msk;
  while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
  NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
  while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
  NVIC_SystemReset();
  }


  bool erase_bonds;

  ret_code_t err_code;

  /* Initialize clock driver for better time accuracy in FREERTOS */
  err_code = nrf_drv_clock_init();
  APP_ERROR_CHECK(err_code);

  // Initialize.
  log_init();
  gpio_init();

  // Configure and initialize the BLE stack.
  ble_stack_init();

  // Initialize modules
  twi_config();
  buttons_leds_init(&erase_bonds);
  serial_uart_init();
  gap_params_init();
  gatt_init();
  advertising_init();
  services_init();
  conn_params_init();
  peer_manager_init();

  // Create a FreeRTOS task for the BLE stack.
  // The task will run advertising_start() before entering its loop.
  nrf_sdh_freertos_init(advertising_start, &erase_bonds);

  // Turn on analog part
  nrf_gpio_pin_set(ANAL_ON);

  // init the LTX6904 at 16.777MHz
  osc_ext_init(6,959);

  set_hp_freq(HP_FREQ_225KHZ);
  nrf_gpio_pin_clear(DP_PORT_SEL);
  nrf_gpio_pin_clear(GAIN_FB);

  // Start Tasks
  init_tasks();

  // Start Timers
  timers_init();
  timers_start();

  vTaskStartScheduler();


  // Enter main loop.
  while(1){
      APP_ERROR_HANDLER(NRF_ERROR_FORBIDDEN);
  }
}


/**
 * @}
 */
