# BioZfirmware Changelogs
## BZfw 0.0.0
- First iteration of BZfw
- FreeRTOS
- Measure always
- Drivers
  - UART
  - TWIM

## BZfw 0.0.1
- Implemented GATT Services
  - DIS
    - Firmware Version
    - Softdevice Version
    - Manufacturer String
  - BAS
    - each 60000ms increment of 1 modulo 100
## BZfw 0.0.2
- Added Nordic UART Service (NUS)
  - Error Invalid Parameters for NUS
  - NUS fonctionning

## BZfw 0.0.3
- Got back to V0.0.1

## BZfw 0.0.4
- App measure only once

## BZfw 0.0.5
- Added NUS
- The firmware send every measure only if nus is connected

## BZfw 0.0.6
- added support for changing pga and output range

## BZfw 0.0.7
- Added support for SD/HC card

## BZfw 0.1.0
- Started from ble_app_template
- Discarded FreeRTOS