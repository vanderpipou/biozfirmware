/**
 * @defgroup SM State Machine header.
 * @{
 *
 * @brief	Header-File for the state machine.
 *
 * This statemachine framework can be used for easy implementing an own
 * state machine. The only thing you need is a two-dimensional state
 * transition table with all states and events.
 *
 *
 *
 * There are some limitations and restrictions to this implementation of a
 * statemachine framework. \n
 * \li A complete event table for each state is nedded. (memory usage)
 *
 * @file	statemachine.h
 * @author	ferdrich
 * @author  changes made by mhuber
 * @author  changes made be dfischer
 * @date	Initial 28.08.2009, last changed on 02.09.2011
 * @version	\b 0.3		SMR Version. \n
 */

#ifndef __STATEMACHINE_H_
#define __STATEMACHINE_H_

#include <stdint.h>
#include <stdbool.h>


/**
 * @brief Set to one, if onEntry() functions are needed.
 */
#ifndef USE_ONENTRY
#define USE_ONENTRY                 1
#endif

/**
 * @brief Set to one, if onDo() functions are needed.
 */
#ifndef USE_ONDO
#define USE_ONDO                    0
#endif

/**
 * @brief Set to one, if onExit functions are needed.
 */
#ifndef USE_ONEXIT
#define USE_ONEXIT                  1
#endif

/**
 * @brief Set to one, if transition functions are nedded.
 */
#ifndef USE_TRANSITIONFUNCTION
#define USE_TRANSITIONFUNCTION      1
#endif


/**
 * @brief MACRO checking if statemachine is in state
 */
#define IS_IN(statemachine, state)  (statemachine.actualState == state)

/** Forward declaration of stateTableEvents */
typedef struct stateTableEvents sStateTable_t;

/**
 * @brief Special states to use with nextStateId in sStateTransitions_t.
 */
typedef enum specialStates
{
    FAULT_STATE = -1,            /*!< Used in sStateTransitions_t as nextStateId for not allowed events. */
	FINAL_STATE = -2             /*!< Used in sStateTransitions_t as nextStateId for the final state. */
} eSpecialStates_t;

/**
 * @brief Special events that can occur while the state machine is running.
 */
typedef enum specialEvents
{
    NO_EVENT = -1,              /*!< Can be used, if there is no event, but the state machine should be called. */
    TIME_EVENT = -2             /*!< Should be used for timer events, especially elapsing timers. */
} eSpecialEvents_t;

/**
 * @brief Callback typedef for better readable code.
 */
typedef void (*callback)(void);

/**
 * @brief Callback for guard functions, that return either true or false.
 */
typedef _Bool (*cbguard)(void);

/**
 * @brief Event table holds events that can generate a state transition.
 *
 * An entry for the event table holds the event, that generates a state
 * transition as well as callbacks for the guard check and the transition
 * function. A transition between two states can only be done, if an event
 * occurs and the guard is true, otherwise the state won't change. If both are
 * true, the transition function will be called, if there is one and the
 * state changes to the nextStateId. A Table should only hold events for one
 * state, so each state should have it's own event table.
 */
typedef struct stateTransition
{
    int32_t eventId;            /*!< Associated event id for this transition. */
    cbguard guardCb;            /*!< Guard function, returns either true or false */
    callback transitionCb;      /*!< Transition function callback. */
    int32_t nextStateId;        /*!< ID of next state. */
} sStateTransitions_t;

/**
 * @brief State table holds states and corresponding event tables.
 *
 * An entry in state table holds the state id for which the entry is created,
 * as well as the event table and three callbacks for functions that are called
 * on state entry, exit and while beeing in the state. Not used functions
 * should be null.
 */
struct stateTableEvents
{
    int32_t stateId;            /*!< ID of this state. */
    callback onEntry;           /*!< onEntry function callback. */
    callback onDo;              /*!< While in the state function callback. */
    callback onExit;            /*!< onExit function callback. */
    sStateTransitions_t* stateEvents; /*!< Event table for this state. */
    sStateTable_t* nestedStateTable;  /*!< If this state machine has a nested state machine */
};

/**
 * @brief All nesseccary data for a state machine.
 *
 * This struct holds all data that is needed by a state machine. Saving the
 * state table is important, because it is needed to identify the state machine.
 */
typedef struct stateMachine
{
    uint32_t smId;              /*!< ID of the state machine, set by init. */

    int32_t actualState;        /*!< Holds the actual state. */
    int32_t nextState;          /*!< Holds the next state after a transition. */

    uint32_t stateCount;        /*!< Number of states in this state machine. */
    uint32_t eventCount;        /*!< Number of events in this state machine. */

	_Bool statechanged;			/*!< Holds if the fsm changed its state after last transition. */

    sStateTable_t* stateTable;  /*!< Table with all states and transitions. */
} sStateMachine_t;

/**
 * @brief Initialization of the State Machine
 *
  */
_Bool statemachine_init(sStateMachine_t* sm, sStateTable_t* st, \
                        int32_t startState, uint32_t stateCount, \
                        uint32_t eventCount, callback initFunction);

/**
 * @brief Dispatch Event
 */
_Bool statemachine_dispatchEvent(sStateMachine_t* sm, int32_t event);


/**
 * @brief Get the actual state of a statemachine
 */
int32_t statemachine_getstate   (sStateMachine_t* sm);


/**
 * @brief Get the statechanged flag. 1 if last event changed the state, 0 if last event did not changed the state
 */
_Bool statemachine_statechanged (sStateMachine_t* sm);


/*@}*/

#endif          /* __STATEMACHINE_H_ */
