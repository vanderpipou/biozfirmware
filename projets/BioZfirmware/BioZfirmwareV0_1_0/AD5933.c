#include "AD5933.h"

uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND ad5933_command[2];  //command
uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND ad5933_byte_w[2];	//byte write
uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND ad5933_bloc_w[12];	//bloc write

uint32_t compute_freq_code(uint32_t target_freq, uint32_t mclk_khz){
  uint32_t code_value = target_freq <<17;
  code_value /= 1000;
  code_value = code_value<<10;
  code_value /= mclk_khz;
  code_value = code_value<<2;
  return code_value;
}

uint8_t sel_range_reg(uint8_t range){
  switch(range){
    case 1:
      return OUT_2VPP;
    case 2:
      return OUT_1VPP;
    case 3:
      return OUT_400MVPP;
    case 4:
      return OUT_200MVPP;
    default:
      return 0;
  }
}

/**@brief Function for setting the address pointer in the AD5933 registers
 *
 * @param[in] pointer_addr	Register address to point
 */
void ad5933_set_pointer(uint8_t pointer_addr, ad5933_mngr_cfg_t mngr_cfg)
{
  ad5933_command[0] = ADDR_POINTER_CMD;	// set command to pointer
  ad5933_command[1] = pointer_addr;	// store the address to point

  // define transfer
  static nrf_twi_mngr_transfer_t ad5933_trsfr[] =
  {
      NRF_TWI_MNGR_WRITE(AD5933_ADDR, &ad5933_command, 2, NRF_TWI_MNGR_NO_STOP)
  };

  // schedule transaction
  ret_code_t err_code;
  err_code = nrf_twi_mngr_perform(mngr_cfg.p_twi_mngr, mngr_cfg.p_twi_config,
				  ad5933_trsfr,
				  sizeof(ad5933_trsfr)/sizeof(ad5933_trsfr[0]),
				  NULL);
  APP_ERROR_CHECK(err_code);
}

uint8_t ad5933_read_byte(ad5933_mngr_cfg_t mngr_cfg){
  uint8_t reg_data;
  // define transfer
   nrf_twi_mngr_transfer_t ad5933_byte_r_trsfr[] =
  {
      NRF_TWI_MNGR_READ(AD5933_ADDR, &reg_data, 1, 0)
  };
  // perform the transfert
  APP_ERROR_CHECK(nrf_twi_mngr_perform(mngr_cfg.p_twi_mngr, mngr_cfg.p_twi_config,
				       ad5933_byte_r_trsfr,
				       sizeof(ad5933_byte_r_trsfr)/sizeof(ad5933_byte_r_trsfr[0]),
				       NULL));
  return reg_data;
}

/**@brief Function for reading a bloc in the AD5933 registers
 *
 * @param[in] pointer_addr	Register address to point
 * @param[in] length  		Length of the bloc to read
 * @param p_data  		Pointer where to store the readings data
 */
void ad5933_read_bloc(uint8_t * p_data, uint8_t length, ad5933_mngr_cfg_t mngr_cfg){
  ad5933_command[0] = BLOCK_READ_CMD;	// set command to pointer
  ad5933_command[1] = length;	// store the address to point

  // define transfer
   nrf_twi_mngr_transfer_t ad5933_bloc_r_trsfr[] =
  {
      NRF_TWI_MNGR_WRITE(AD5933_ADDR, &ad5933_command, 2, NRF_TWI_MNGR_NO_STOP),
      NRF_TWI_MNGR_READ(AD5933_ADDR, p_data, length, 0)
  };
  // perform the transfert
  APP_ERROR_CHECK(nrf_twi_mngr_perform(mngr_cfg.p_twi_mngr, mngr_cfg.p_twi_config,
				       ad5933_bloc_r_trsfr,
				       sizeof(ad5933_bloc_r_trsfr)/sizeof(ad5933_bloc_r_trsfr[0]),
				       NULL));
}

/**@brief Function for writing a byte in the AD5933 registers
 *
 * @param[in] addr_reg		Register address
 * @param[in] data  		Data to write
 */
void ad5933_write_byte(uint8_t addr_reg, uint8_t data, ad5933_mngr_cfg_t mngr_cfg){
  ad5933_byte_w[0] = addr_reg;
  ad5933_byte_w[1] = data;

  // define transfer
  nrf_twi_mngr_transfer_t ad5933_byte_w_trsfr[] =
  {
      NRF_TWI_MNGR_WRITE(AD5933_ADDR, &ad5933_byte_w, 2, 0)
  };

  APP_ERROR_CHECK(nrf_twi_mngr_perform(mngr_cfg.p_twi_mngr, mngr_cfg.p_twi_config,
				   ad5933_byte_w_trsfr,
				   sizeof(ad5933_byte_w_trsfr)/sizeof(ad5933_byte_w_trsfr[0]),
				   NULL));
}

void ad5933_write_bloc(uint8_t * p_data, uint8_t length, ad5933_mngr_cfg_t mngr_cfg){
  ad5933_bloc_w[0] = BLOCK_WRITE_CMD;	// set command to block write
  ad5933_bloc_w[1] = length;		// set the length of the bloc

  for(uint8_t i = 0;i<length;i++){
      ad5933_bloc_w[i+2]=p_data[i];
  }

  // define transfer
  nrf_twi_mngr_transfer_t ad5933_bloc_w_trsfr[] =
  {
      NRF_TWI_MNGR_WRITE(AD5933_ADDR, &ad5933_bloc_w, length+2, 0)
  };
  APP_ERROR_CHECK(nrf_twi_mngr_perform(mngr_cfg.p_twi_mngr, mngr_cfg.p_twi_config,
				   ad5933_bloc_w_trsfr,
  				   sizeof(ad5933_bloc_w_trsfr)/sizeof(ad5933_bloc_w_trsfr[0]),
  				   NULL));
}


void ad5933_set_freq(uint32_t freq_code, ad5933_mngr_cfg_t mngr_cfg){
  uint8_t regs[3];
  regs[0] = (uint8_t)(freq_code>>16);
  regs[1] = (uint8_t)(freq_code>>8);
  regs[2] = (uint8_t)(freq_code);

  ad5933_set_pointer(START_REG_OFFSET,mngr_cfg);

  ad5933_write_bloc(&regs[0],sizeof(regs),mngr_cfg);
}

void ad5933_set_freq_inc(uint32_t freq_inc_code, ad5933_mngr_cfg_t mngr_cfg){
  uint8_t regs[3];
  regs[0] = (uint8_t)(freq_inc_code>>16);
  regs[1] = (uint8_t)(freq_inc_code>>8);
  regs[2] = (uint8_t)(freq_inc_code);

  ad5933_set_pointer(FREQ_INC_REG_OFFSET,mngr_cfg);
  ad5933_write_bloc(&regs[0],sizeof(regs),mngr_cfg);
}

void ad5933_set_settling(uint16_t nb_settling, uint8_t sett_mult, ad5933_mngr_cfg_t mngr_cfg){
  uint8_t regs[2];

  if((sett_mult != SETT_MULT_DEFAULT)&&(sett_mult != SETT_MULT_X2)&&(sett_mult != SETT_MULT_X4)){
      sett_mult = SETT_MULT_DEFAULT;
  }

  regs[0] = (sett_mult<<1)|((nb_settling>>8)&0x01);
  regs[1] = (uint8_t)(nb_settling);

  ad5933_set_pointer(NB_SET_REG_OFFSET,mngr_cfg);
  ad5933_write_bloc(&regs[0],sizeof(regs),mngr_cfg);
}

void ad5933_set_inc(uint16_t nb_inc, ad5933_mngr_cfg_t mngr_cfg){
  uint8_t regs[2];

  regs[0] = (uint8_t)((nb_inc>>8)&0x01);
  regs[1] = (uint8_t)(nb_inc);

  ad5933_set_pointer(NB_INC_REG_OFFSET,mngr_cfg);
  ad5933_write_bloc(&regs[0],sizeof(regs),mngr_cfg);

}

uint8_t ad5933_get_status(ad5933_mngr_cfg_t mngr_cfg){
  ad5933_set_pointer(STATUS_REG_OFFSET,mngr_cfg);
  return ad5933_read_byte(mngr_cfg);
}

int16_t ad5933_get_data(uint8_t addr,ad5933_mngr_cfg_t mngr_cfg){
  uint8_t regs[2];
  ad5933_set_pointer(addr,mngr_cfg);
  ad5933_read_bloc(&regs[0],2,mngr_cfg);
  return (int16_t)((regs[0]<<8)|(regs[1]));
}

void ad5933_set_mode(uint8_t mode, ad5933_mngr_cfg_t mngr_cfg){
  uint8_t reg;
  ad5933_set_pointer(CTRL_REG_OFFSET,mngr_cfg);
  reg = ad5933_read_byte(mngr_cfg);
  reg = (reg&0x0F)|mode;
  ad5933_write_byte(CTRL_REG_OFFSET,reg,mngr_cfg);
}

void ad5933_pga_gain(uint8_t gain, ad5933_mngr_cfg_t mngr_cfg){
  uint8_t reg;
  ad5933_set_pointer(CTRL_REG_OFFSET,mngr_cfg);
  reg = ad5933_read_byte(mngr_cfg);
  reg = (reg&0xFE)|gain;
  ad5933_write_byte(CTRL_REG_OFFSET,reg,mngr_cfg);
}

void ad5933_output_range(uint8_t range, ad5933_mngr_cfg_t mngr_cfg){
  uint8_t reg;
  ad5933_set_pointer(CTRL_REG_OFFSET,mngr_cfg);
  reg = ad5933_read_byte(mngr_cfg);
  reg = (reg&0xF6)|range;
  ad5933_write_byte(CTRL_REG_OFFSET,reg,mngr_cfg);
}

void ad5933_reset(ad5933_mngr_cfg_t mngr_cfg){
  ad5933_write_byte(CTRL_REG_OFFSET+1,SOFT_RESET,mngr_cfg);
}

void ad5933_use_ext_clk(bool use_ext_clk, ad5933_mngr_cfg_t mngr_cfg){
  if(use_ext_clk){
      ad5933_write_byte(CTRL_REG_OFFSET+1,USE_EXT_CLOCK,mngr_cfg);
  }
  else{
      ad5933_write_byte(CTRL_REG_OFFSET+1,0,mngr_cfg);
  }
}











