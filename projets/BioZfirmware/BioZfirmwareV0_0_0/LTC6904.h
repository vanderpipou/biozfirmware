/******************************************************************************/
/*	Name:	LTC6904.h						      */
/*	Author: Steve Mermet						      */
/*	Date:	11.10.2019						      */
/*									      */
/******************************************************************************/

#ifndef LTC6904_H
#define LTC6904_H

#include <stdint.h>
#include "nrf_twi_mngr.h"

#ifdef __cplusplus
extern "C" {
#endif

// Define if pin ADR is High or Low
#define ADR_PIN		0

#if !ADRPIN
#define LTC6904_ADDR	0b0010111
#else
#define LTC6904_ADDR	0b0010110
#endif

#define OCT_DAC_TO_MSB(oct, dac) (uint8_t)(((oct&0x0F)<<4)|((dac&0x03C0)>>6))
#define DEC_CFN_TO_MSB(dac, cfn) (uint8_t)(((dac&0x3F)<<2)|(cfn))

typedef struct{
  uint8_t	oct;
  uint16_t	dac;
  uint8_t	cfn;
  nrf_twi_mngr_t const *	p_twi_mngr;
  nrf_drv_twi_config_t const *	p_twi_config;
}ltc6904_cfg_t;

void ltc6904_set_freq(ltc6904_cfg_t);
uint8_t ltc6904_oct_compute(uint32_t);
uint16_t ltc6904_dac_compute(uint32_t,uint8_t);

#ifdef __cplusplus
}
#endif

#endif//LTC6904_H
