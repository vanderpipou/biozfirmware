#include <stdio.h>
#include "fsm-framework.h"

/* Counter for number of state machines. */
static uint32_t smCount;

_Bool statemachine_init(sStateMachine_t* sm, sStateTable_t* st, \
                        int32_t startState, uint32_t stateCount, \
                        uint32_t eventCount, callback initFunction)
{
    /* call initFunction */
    if (initFunction)
        initFunction();
    
    /* check for proper parameters */

    /* save unique id into state machine struct */
    sm->smId = ++smCount;

    /* store startState */
    sm->actualState = startState;
    sm->nextState = startState;

    /* store number of state and events */
    sm->stateCount = stateCount;
    sm->eventCount = eventCount;

    /* store state table reference */
    sm->stateTable = st;

	/* No state change at the beginning */
	sm->statechanged = false;

   /* Call entry function of first state */
   if (sm->stateTable[startState].onEntry != 0)
   {
      sm->stateTable[startState].onEntry();
   }

    /* maybe do a check for correct tables */
    /* add call to onEntry function of startState */

    return true;
}

_Bool statemachine_dispatchEvent(sStateMachine_t* sm, int32_t event)
{
    sStateTable_t *st = sm->stateTable;
    sStateTransitions_t *strans = st[sm->actualState].stateEvents;

    if (sm == 0)
        return (sm->statechanged = false);

    switch (event)
    {
        case NO_EVENT:
#if USE_ONDO
            if (st[sm->actualState].onDo)
                st[sm->actualState].onDo();
#endif          /* USE_ONDO */
            break;
        case TIME_EVENT:
            break;
        default:
			/* check, if statemachine is already in final state */
            if (IS_IN((*sm), FINAL_STATE))
			{
                return (sm->statechanged=false);
			}

            /* check, if transition with this event is defined */
            if (strans[event].nextStateId == FAULT_STATE)
			{
                return (sm->statechanged=false);
			}

            /* check for guard callback and run function, if exists */
            if (strans[event].guardCb)
            {
                if (strans[event].guardCb() == false)
                    return sm->statechanged=false;
            }

            sm->nextState = strans[event].nextStateId;

#if USE_ONEXIT
            /* onExit handler */
            if (st[sm->actualState].onExit)
                st[sm->actualState].onExit();
#endif          /* USE_ONEXIT */

#if USE_TRANSITIONFUNCTION
            if (strans[event].transitionCb)
                strans[event].transitionCb();
#endif          /* USE_TRANSITIONFUNCTION */

#if USE_ONENTRY
            /* call onEntry */
            if (st[sm->nextState].onEntry)
                st[sm->nextState].onEntry();
#endif          /* USE_ONENTRY */

            /* make nextStateId to actual stateId */
            sm->actualState = sm->nextState;

#if USE_ONDO
            /* call onDo() */
            if (st[sm->actualState].onDo)
                st[sm->nextState].onDo();
#endif          /* USE_ONDO */

            break;

    }

    return (sm->statechanged=true);
}

int32_t statemachine_getstate(sStateMachine_t* sm)
{
	return sm->actualState;
}

_Bool statemachine_statechanged (sStateMachine_t* sm)
{
	return sm->statechanged;
}
