
#include "main.h"


/************************** BLE GAP/GATT **************************************/
#define DEVICE_NAME                     "MEA-Z-ure_v4_0"                       	/**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "HEPIA"                   		/**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                300                                     /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */

#define APP_ADV_DURATION                0                                   /**< The advertising duration (180 seconds) in units of 10 milliseconds. */
#define APP_BLE_OBSERVER_PRIO           3                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG            1                                       /**< A tag identifying the SoftDevice BLE configuration. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                   0                                       /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                   /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                  1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                       /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                      /**< Maximum encryption key size. */

#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

//#define DEBUG
//#define TWI_SCHEDULE
//#define SEND_HELLO
//#define LOG_STATE

/************************** Save measure **************************************/
static uint32_t meas_save[50];
static void meas_save_init(void){
  memset(&meas_save, 0, sizeof(meas_save));
}
/************************** OSC EXTERN ****************************************/
#define OSC_OCT_1HZ			6
#define OSC_OCT_100HZ			9
#define OSC_OCT_1KHZ			13

#define OSC_DAC				959

#define BATT_LVL_MEAS_INTERVAL		1000					/**< Battery level measure timer interval in ms. */

NRF_SERIAL_UART_DEF(m_nrf_uart_id, 0);						/**< Application Serial UART config definition. */
NRF_SERIAL_DRV_UART_CONFIG_DEF(m_uart0_drv_config,
			       RX_PIN_NUMBER, TX_PIN_NUMBER,
			       RTS_PIN_NUMBER, CTS_PIN_NUMBER,
			       NRF_UART_HWFC_DISABLED,
			       NRF_UART_PARITY_EXCLUDED,
			       NRF_UART_BAUDRATE_115200,
			       UART_DEFAULT_CONFIG_IRQ_PRIORITY);		/**< Application Serial UART Driver config definition */

#define SERIAL_FIFO_TX_SIZE 32
#define SERIAL_FIFO_RX_SIZE 32

NRF_SERIAL_QUEUES_DEF(serial_queues, SERIAL_FIFO_TX_SIZE, SERIAL_FIFO_RX_SIZE);


#define SERIAL_BUFF_TX_SIZE 1
#define SERIAL_BUFF_RX_SIZE 1

NRF_SERIAL_BUFFERS_DEF(serial_buffs, SERIAL_BUFF_TX_SIZE, SERIAL_BUFF_RX_SIZE);

NRF_SERIAL_CONFIG_DEF(serial_config, NRF_SERIAL_MODE_IRQ,
                      &serial_queues, &serial_buffs, NULL, NULL);



/*****************************************************************************/
/*                        TWI MANAGER                                        */
/*                                                                           */
/*****************************************************************************/
#define TWIM_QUEUE_SIZE 32

NRF_TWI_MNGR_DEF(m_twi_mngr, TWIM_QUEUE_SIZE, 0);				/**< Definition of the TWI Manager */
nrf_drv_twi_config_t const m_twi_config = {
      .scl		= TWIM0_SCL,
      .sda		= TWIM0_SDA,
      .frequency	= NRF_DRV_TWI_FREQ_100K,
      .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
      .clear_bus_init	= true
  };

static void twi_config(void)
{
  APP_ERROR_CHECK(nrf_twi_mngr_init(&m_twi_mngr, &m_twi_config));
}

/************************** Tasks and Timers **********************************/
#if NRF_LOG_ENABLED
static TaskHandle_t m_logger_thread;                                		/**< Definition of Logger thread. */
#endif

static TimerHandle_t m_battery_timer;						/**< Definition of the battery timer. */

static TaskHandle_t m_imp_analyzer_task;
static TaskHandle_t m_main_task;

/************************** Gloabl Varialble **********************************/

#define DECADE_BEGIN	0
#define FREQ_OSC	1000000

static measure_cb_t current_meas = {
    .m_running = true,
    .m_current_state = config,
    .m_ad5933_mngr = {
	  .p_twi_mngr = &m_twi_mngr,
	  .p_twi_config = &m_twi_config
    },
    .m_current_decade = DECADE_BEGIN,
    .m_current_inc = 0
};

/*****************************************************************************/
/*                        SD Memory Card                                     */
/*                                                                           */
/*****************************************************************************/
/*
void sdc_handler(sdc_evt_t const * p_event){
  switch(p_event->type){
    case SDC_EVT_INIT:
      APP_ERROR_CHECK(p_event->result);
      break;
    case SDC_EVT_READ:
      APP_ERROR_CHECK(p_event->result);
      break;
    case SDC_EVT_WRITE:
      APP_ERROR_CHECK(p_event->result);
    default:break;
  }
  return;
}

uint8_t buffer[SDC_SECTOR_SIZE];

static const app_sdc_config_t sdc_cfg = {
    .mosi_pin = SPIM0_MOSI_PIN,
    .miso_pin = SPIM0_MISO_PIN,
    .sck_pin  = SPIM0_SCK_PIN,
    .cs_pin   = SPIM0_CS_PIN
};

static void sd_card_init(void){
  APP_ERROR_CHECK(app_adc_init(&sdc_cfg, sdc_handler));
  while(app_sdc_busy_check()){
      wait();
  }
}*/


/*****************************************************************************/
/*                       LTC6904 Configuration                               */
/*                                                                           */
/*****************************************************************************/

uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND osc_ext_reg[2];

#define CFN	0x02	// Output CLK On, Output nCLK Off

static void osc_ext_callback(ret_code_t result, void *p_user_data)
{
  //bsp_board_led_invert(1);
  if (result != NRF_SUCCESS)
  {
      NRF_LOG_WARNING("osc_ext_callback - error: %d", (int)result);
      return;
  }
  //bsp_board_led_invert(1);
  NRF_LOG_INFO("OSC EXT Configured");
  NRF_LOG_HEXDUMP_INFO(&osc_ext_reg, 2);

}

static void osc_ext_init(uint8_t oct, uint16_t dac)
{
  if(nrf_gpio_pin_read(ANAL_ON) == 0){
      return;
  }
  //fill reg
  osc_ext_reg[0] = OCT_DAC_TO_MSB(oct, dac);
  osc_ext_reg[1] = DEC_CFN_TO_MSB(dac,CFN);
  // define transfert
  static nrf_twi_mngr_transfer_t osc_ext_trsfr[] =
  {
      NRF_TWI_MNGR_WRITE(LTC6904_ADDR, &osc_ext_reg, 2, 0)
  };


  // define transaction
  static nrf_twi_mngr_transaction_t NRF_TWI_MNGR_BUFFER_LOC_IND transaction =
  {
      .callback 		= osc_ext_callback,
      .p_user_data 		= NULL,
      .p_transfers 		= osc_ext_trsfr,
      .number_of_transfers 	= sizeof(osc_ext_trsfr)/sizeof(osc_ext_trsfr[0])
  };
  //schedule transaction
  APP_ERROR_CHECK(nrf_twi_mngr_schedule(&m_twi_mngr, &transaction));
}

/*****************************************************************************/
/*                 Battery Level Measure Timer                               */
/*                                                                           */
/*****************************************************************************/

static void  battery_level_meas_timeout_handler(void * pvParameter){
  UNUSED_PARAMETER(pvParameter);
  //NRF_LOG_INFO("hguetihuger");
  bsp_board_led_invert(1);
}

/*****************************************************************************/
/*                        Measure Task                                       */
/*                                                                           */
/*****************************************************************************/

static void imp_analyzer_task(void * v_params){
  UNUSED_PARAMETER(v_params);

 /* static ad5933_mngr_cfg_t imp_ana_mngr = {
      .p_twi_mngr = &m_twi_mngr,
      .p_twi_config = &m_twi_config
  };*/

  static ltc6904_cfg_t osc_ext_cfg = {
      .oct = 9,
      .dac = 959,
      .p_twi_mngr = &m_twi_mngr,
      .p_twi_config = &m_twi_config
  };

  static uint8_t meas_num = 0;

  uint8_t temp_status = 0;
  uint32_t freq_start = 1;
  uint32_t freq_osc = FREQ_OSC;

  uint32_t freq_code = 0;
  int16_t data[2] = {0,0};

  while(1){
      if(current_meas.m_running){
	  nrf_gpio_pin_set(ANAL_ON);
	  switch(current_meas.m_current_state){
	    case config:
	      NRF_LOG_INFO("decade: %u",current_meas.m_current_decade);

	      osc_ext_cfg.oct = ltc6904_oct_compute(freq_osc);
	      osc_ext_cfg.dac = ltc6904_dac_compute(freq_osc, osc_ext_cfg.oct);

	      freq_code = compute_freq_code(freq_start,freq_osc/1000);

	      ltc6904_set_freq(osc_ext_cfg);
	      ad5933_use_ext_clk(true, current_meas.m_ad5933_mngr);
	      ad5933_pga_gain(PGA_GAIN_1, current_meas.m_ad5933_mngr);
	      ad5933_output_range(OUT_200MVPP, current_meas.m_ad5933_mngr);
	      ad5933_set_freq(freq_code,current_meas.m_ad5933_mngr);
	      ad5933_set_freq_inc(freq_code,current_meas.m_ad5933_mngr);
	      ad5933_set_settling(10,SETT_MULT_DEFAULT,current_meas.m_ad5933_mngr);
	      ad5933_set_inc(8,current_meas.m_ad5933_mngr);
	      current_meas.m_current_state = standby;
	      break;

	    case standby:
	      //NRF_LOG_INFO("standby");
	      ad5933_set_mode(STANDBY_MODE, current_meas.m_ad5933_mngr);
	      current_meas.m_current_state = init;
	      break;

	    case init:
	      //NRF_LOG_INFO("init");
	      ad5933_set_mode(INIT_START_FREQ, current_meas.m_ad5933_mngr);
	      vTaskDelay(2000);
	      current_meas.m_current_state = start;
	      break;

	    case start:
	      //NRF_LOG_INFO("start");
	      ad5933_set_mode(START_FREQ_SWEEP, current_meas.m_ad5933_mngr);
	      current_meas.m_current_state = sweep;
	      break;

	    case sweep:
	      //NRF_LOG_INFO("sweep");
	      temp_status = ad5933_get_status(current_meas.m_ad5933_mngr);
	      if(temp_status & IMP_MEAS_VALID){
		  //NRF_LOG_INFO("measure:");
		  data[0] = ad5933_get_data(REAL_REG_OFFSET, current_meas.m_ad5933_mngr);
		  data[1] = ad5933_get_data(IMAG_REG_OFFSET, current_meas.m_ad5933_mngr);
		  meas_num = (current_meas.m_current_decade*9) +current_meas.m_current_inc;
		  //NRF_LOG_INFO("Meas num: %d",meas_num);
		  meas_save[meas_num]=(uint32_t)((data[0]<<16)|data[1]);
		  //NRF_LOG_INFO("real %d - imag %d",data[0],data[1]);
		  if(temp_status & FREQ_SWEEP_COMPLETE){
		      current_meas.m_current_inc = 0;
		      current_meas.m_current_decade++;
		      if(current_meas.m_current_decade >= 5){
			  current_meas.m_current_state = done;
		      }
		      else{
			  current_meas.m_current_state = decade;
		      }
		      //current_meas.m_current_decade %= 5;
		  }
		  else{
		      ad5933_set_mode(INC_FREQ, current_meas.m_ad5933_mngr);
		      current_meas.m_current_state = sweep;
		      current_meas.m_current_inc++;
		  }
	      }
	      break;

	    case decade:
	      //bsp_board_led_off(0);
	      freq_start = 1;
	      for(uint8_t i = 0;i<current_meas.m_current_decade;i++){
		  freq_start *= 10;
	      }
	      freq_osc = FREQ_OSC<<(current_meas.m_current_decade);
	      ad5933_set_mode(STANDBY_MODE, current_meas.m_ad5933_mngr);
	      vTaskDelay(500);
	      current_meas.m_current_state = config;
	      break;
	    case done:
	      current_meas.m_running = false;
	      current_meas.m_current_state = decade;
	      current_meas.m_current_decade = 0;
	      current_meas.m_current_inc = 0;
	      break;

	    default:
	      current_meas.m_current_decade = done;
	      break;
	  }
	  bsp_board_led_invert(0);
      }
      else {
	  nrf_gpio_pin_clear(ANAL_ON);
	  bsp_board_led_on(0);
	  NRF_LOG_INFO("Impedance not running");
      }

      vTaskDelay(500);
  }
}



/*****************************************************************************/
/*                        Timer Configuration                                */
/*                                                                           */
/*****************************************************************************/
/*
typedef enum {config, standby, wait_settling, poll_status, done}ad5933_state_t;

typedef struct {
  ad5933_state_t current_state;	// current state of the state machine
  uint32_t start_freq;		// start frequency in Hz
  uint32_t freq_increment;	// frequency increment in Hz
  uint16_t increments;		// number of frequency increment
  uint16_t settling;		// number of settling cycles
  uint8_t  output_range;	// output range
  uint8_t  pga_gain;		// pga gain
} sm_ad5933_t;*/


static void timers_init(void){
  ret_code_t err_code = app_timer_init();
  APP_ERROR_CHECK(err_code);

  // Create Timer
  m_battery_timer = xTimerCreate("BATT",
				 BATT_LVL_MEAS_INTERVAL,
				 pdTRUE,
				 NULL,
				 battery_level_meas_timeout_handler);
}

static void timers_start(void){
  UNUSED_VARIABLE(xTimerStart(m_battery_timer, 0));
}


/**@brief Function for the Serial UART initialization.
 *
 * @details Initializes the Serial URAT module.
 */
static void serial_uart_init(void)
{
  // Initialize UART module.
  ret_code_t err_code;
  err_code = nrf_serial_init(&m_nrf_uart_id, &m_uart0_drv_config, &serial_config);
  APP_ERROR_CHECK(err_code);
}


/*****************************************************************************/
/*                        Initialisations                                    */
/*                                                                           */
/*****************************************************************************/

/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    nrf_gpio_pin_clear(ANAL_ON);

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated when button is pressed.
 */
static void bsp_event_handler(bsp_event_t event)
{
    //ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break; // BSP_EVENT_SLEEP

        default:
            break;
    }
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
    ret_code_t err_code;

    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

#if NRF_LOG_ENABLED
/**@brief Thread for handling the logger.
 *
 * @details This thread is responsible for processing log entries if logs are deferred.
 *          Thread flushes all log entries and suspends. It is resumed by idle task hook.
 *
 * @param[in]   arg   Pointer used for passing some arbitrary information (context) from the
 *                    osThreadCreate() call to the thread.
 */
static void logger_thread(void * arg)
{
    UNUSED_PARAMETER(arg);

    while (1)
    {
        NRF_LOG_FLUSH();

        vTaskSuspend(NULL); // Suspend myself
    }
}
#endif //NRF_LOG_ENABLED

static void main_task(void * v_params);

/**@brief A function which is hooked to idle task.
 * @note Idle hook must be enabled in FreeRTOS configuration (configUSE_IDLE_HOOK).
 */
void vApplicationIdleHook( void )
{
#if NRF_LOG_ENABLED
     vTaskResume(m_logger_thread);

#endif
}

static void init_tasks(void){

#if NRF_LOG_ENABLED
  // Start execution.
  if (pdPASS != xTaskCreate(logger_thread, "LOGGER",
			    configMINIMAL_STACK_SIZE,
			    NULL, 2, &m_logger_thread))
  {
      APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
#endif

  if(pdPASS != xTaskCreate(imp_analyzer_task, "IMPANALYZER",
			   configMINIMAL_STACK_SIZE,
			   NULL, 1, &m_imp_analyzer_task)){
      APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }

  if(pdPASS != xTaskCreate(main_task, "MAIN",
  			   configMINIMAL_STACK_SIZE,
  			   NULL, 1, &m_main_task)){
      APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
}


/**@brief Function for application main entry.
 */
int main(void){
  // Configure the pins NFCT P9 and P10 as GPIO otherwise the pins are used as NFCT
  if ((NRF_UICR->NFCPINS & UICR_NFCPINS_PROTECT_Msk) == (UICR_NFCPINS_PROTECT_NFC << UICR_NFCPINS_PROTECT_Pos)){
  NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
  while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
  NRF_UICR->NFCPINS &= ~UICR_NFCPINS_PROTECT_Msk;
  while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
  NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
  while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
  NVIC_SystemReset();
  }


  bool erase_bonds;

  ret_code_t err_code;

  /* Initialize clock driver for better time accuracy in FREERTOS */
  err_code = nrf_drv_clock_init();
  APP_ERROR_CHECK(err_code);

  // Initialize.
  log_init();
  gpio_init();
  twi_config();
  buttons_leds_init(&erase_bonds);
  serial_uart_init();
  meas_save_init();

  // Start Tasks
  init_tasks();

  vTaskStartScheduler();


  // Enter main loop.
  while(1){
      APP_ERROR_HANDLER(NRF_ERROR_FORBIDDEN);
  }
}

static void main_task(void * v_params){
  // Turn on analog part
    nrf_gpio_pin_set(ANAL_ON);

    // init the LTX6904 at 16.777MHz
    osc_ext_init(6,959);

    set_hp_freq(HP_FREQ_225KHZ);
    nrf_gpio_pin_clear(DP_PORT_SEL);
    nrf_gpio_pin_clear(GAIN_FB);

    set_output(0);

    // Start Timers
    timers_init();
    timers_start();

    NRF_LOG_INFO("meas: 0x%X",&meas_save);

    while(1){
	if(current_meas.m_running == true){
	    vTaskDelay(1000);
	}
	else{
	    current_meas.m_running = true;
	}
    }

}


/**
 * @}
 */
