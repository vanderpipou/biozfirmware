/**
 * Copyright (c) 2009 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
* @brief Example template project.
* @defgroup nrf_templates_example Example Template
*
*/

#include <stdio.h>
#include <stdlib.h>
#include "boards.h"
#include "nrf_delay.h"
#include "app_error.h"
#include "app_util_platform.h"
#include "nrf_drv_twi.h"
#include "nrf_twi_mngr.h"

#include "app_uart.h"
#include "nrf_uart.h"

#include "nrf.h"
#include "bsp.h"
#include "nrf_gpio.h"

#include "nrf_drv_timer.h"
#include "nrfx_timer.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

//#include "SEGGER_RTT.h"

//#define EXEMPLE_TWI
//#define EXEMPLE_UART
#define EXEMPLE_TIMER
//#define LOG

#ifdef EXEMPLE_TIMER
/* ---------- TIMER ---------- */
const nrfx_timer_t TIMER_LED = 	NRFX_TIMER_INSTANCE(1);

void timer_led_handler(nrf_timer_event_t ev_type, void* p_context){
  bsp_board_led_on(1);
  switch(ev_type){
    case NRF_TIMER_EVENT_COMPARE0:
      // do nothing
      break;
    case NRF_TIMER_EVENT_COMPARE1:
      //do nothing
      break;
    case NRF_TIMER_EVENT_COMPARE2:
      // do nothing
      break;
    case NRF_TIMER_EVENT_COMPARE3:
      // do nothing
      break;
    case NRF_TIMER_EVENT_COMPARE4:
      // do nothing
      break;
    case NRF_TIMER_EVENT_COMPARE5:
      // do nothing
      break;
    default:
      // do nothing
      break;
  }
  //nrfx_timer_clear(&TIMER_LED);
}

void timer_init(uint32_t periode_ms){

  uint32_t time_ticks;
  // config timer
  nrfx_timer_config_t timer_cfg = {
      .frequency	= NRF_TIMER_FREQ_500kHz ,
      .mode		= NRF_TIMER_MODE_TIMER,
      .bit_width	= NRF_TIMER_BIT_WIDTH_32,
      .interrupt_priority	= NRFX_TIMER_DEFAULT_CONFIG_IRQ_PRIORITY,
      .p_context	= NULL
  };
  // init timer
  APP_ERROR_CHECK(nrfx_timer_init(&TIMER_LED, &timer_cfg,timer_led_handler));
  //transform ms in system ticks
  time_ticks = nrfx_timer_ms_to_ticks(&TIMER_LED, periode_ms);
  // config timer periode
  nrfx_timer_extended_compare(&TIMER_LED, NRF_TIMER_CC_CHANNEL1, time_ticks, NRF_TIMER_SHORT_COMPARE1_CLEAR_MASK, true);
  // start timer
  nrfx_timer_disable(&TIMER_LED);
  nrfx_timer_clear(&TIMER_LED);
  nrfx_timer_enable(&TIMER_LED);

}
#endif
#ifdef EXEMPLE_TWI
/* ---------- TWI ---------- */

/* TWI instance ID. */
#if TWI0_ENABLED
#define TWI_INSTANCE_ID     0
#elif TWI1_ENABLED
#define TWI_INSTANCE_ID     1
#endif

#define TWI_MNGR_BUF_SIZE	5
#define TWI_SENSOR_BUF_SIZE	8

/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
//static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

NRF_TWI_MNGR_DEF(m_nrf_twi_mngr, TWI_MNGR_BUF_SIZE, TWI_INSTANCE_ID);


/* Buffer for samples read from temperature sensor. */
//static uint8_t m_sample;

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
        	NRF_LOG_INFO("Test.");
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

// TWI with transaction manager init
static void twi_config(void)
{
  uint32_t err_code;

  nrf_drv_twi_config_t const twi_config = {
      .scl	  	  = TWIM0_SCL,
      .sda	  	  = TWIM0_SDA,
      .frequency	  = NRF_DRV_TWI_FREQ_100K,
      .interrupt_priority = APP_IRQ_PRIORITY_LOWEST,
      .clear_bus_init     = false
  };

//  err_code = nrf_drv_twi_init(&m_twi, &twi_config, twi_handler, NULL);
  err_code = nrf_twi_mngr_init(&m_nrf_twi_mngr, &twi_config);
  APP_ERROR_CHECK(err_code);

//  nrf_drv_twi_enable(&m_twi);
}
#endif
#ifdef EXEMPLE_UART
/* ---------- UART ---------- */
#define UART_TX_BUF_SIZE	256
#define UART_RX_BUF_SIZE	256

void uart_error_handle(app_uart_evt_t * p_event){
  //bsp_board_led_invert(1);
  if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR){
      APP_ERROR_HANDLER(p_event->data.error_communication);
  }
  else if (p_event->evt_type == APP_UART_FIFO_ERROR){
      APP_ERROR_HANDLER(p_event->data.error_code);
  }
}

void uart_init(void){

  ret_code_t err_code;

  // build params for uart communication
  const app_uart_comm_params_t comm_params ={
      RX_PIN_NUMBER,
      TX_PIN_NUMBER,
      RTS_PIN_NUMBER,
      CTS_PIN_NUMBER,
      APP_UART_FLOW_CONTROL_DISABLED,
      false,
      NRF_UART_BAUDRATE_115200
  };
  // init uart fifo stack
  APP_UART_FIFO_INIT(&comm_params,
		     UART_RX_BUF_SIZE,
		     UART_TX_BUF_SIZE,
		     NULL,
		     APP_IRQ_PRIORITY_LOWEST,
		     err_code);
  APP_ERROR_CHECK(err_code);
}
#endif

/* ---------- GPIO ---------- */
void gpio_init(void){

  /* Setting outputs */
  nrf_gpio_cfg_output(DP_PORT_SEL);
  nrf_gpio_cfg_output(IMP_CTRL0);
  nrf_gpio_cfg_output(IMP_CTRL1);
  nrf_gpio_cfg_output(OUT_SEL0);
  nrf_gpio_cfg_output(OUT_SEL1);
  nrf_gpio_cfg_output(OUT_SEL2);
  nrf_gpio_cfg_output(OUT_SEL3);
  nrf_gpio_cfg_output(OUT_SEL4);
  nrf_gpio_cfg_output(GAIN_FB);
  nrf_gpio_cfg_output(ANAL_ON);
  nrf_gpio_cfg_output(DP_SEL_P);
  nrf_gpio_cfg_output(DP_SEL_N);
  /* Setting inputs */
  nrf_gpio_cfg_input(BAT_STATUS, NRF_GPIO_PIN_NOPULL);
  nrf_gpio_cfg_input(DTR_PIN_NUMBER, NRF_GPIO_PIN_NOPULL);
  /* Clearing outputs */
  nrf_gpio_pin_clear(DP_PORT_SEL);
  nrf_gpio_pin_clear(IMP_CTRL0);
  nrf_gpio_pin_clear(IMP_CTRL1);
  nrf_gpio_pin_clear(OUT_SEL0);
  nrf_gpio_pin_clear(OUT_SEL1);
  nrf_gpio_pin_clear(OUT_SEL2);
  nrf_gpio_pin_clear(OUT_SEL3);
  nrf_gpio_pin_clear(OUT_SEL4);
  nrf_gpio_pin_clear(GAIN_FB);
  nrf_gpio_pin_clear(ANAL_ON);
  nrf_gpio_pin_clear(DP_SEL_P);
  nrf_gpio_pin_clear(DP_SEL_N);

}

#ifdef LOG
void log_init(void)
{
    ret_code_t err_code;

    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
    NRF_LOG_INFO("Log init");
    NRF_LOG_PROCESS();
    //SEGGER_RTT_Init();
}
#endif

int main(void){

  //ret_code_t err_code;
#ifdef LOG
  log_init();
#endif
  bsp_board_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS);

  gpio_init();

#ifdef EXEMPLE_TIMER
  timer_init(1000);
#endif

#ifdef EXEMPLE_TWI
  twi_config();
#endif

#ifdef EXEMPLE_UART
  uart_init();
#endif

  //NRF_LOG_INFO("INVERTED");
  //NRF_LOG_PROCESS();

  /* Toggle LEDs. */
  while (true)
  {
      nrf_delay_ms(200);
      bsp_board_led_invert(0);

  }
}
