/*
 * This program use the twi driver to read the registers of the LTC2941
 *
 *  Created on: 20 sept. 2019
 *      Author: Steve Mermet
 */

#include <stdio.h>
#include <stdlib.h>
#include "boards.h"
#include "nrf_delay.h"
#include "app_error.h"
#include "app_util_platform.h"
#include "nrf_drv_twi.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* TWI instance ID. */
#if TWI0_ENABLED
#define TWI_INSTANCE_ID     0
#elif TWI1_ENABLED
#define TWI_INSTANCE_ID     1
#endif

/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

/* Buffer for samples read from temperature sensor. */
//static uint8_t m_sample;

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
                //data_handler(m_sample);
        	NRF_LOG_INFO("Temperature: %d Celsius degrees.");
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

// TWI with transaction manager init
static void twi_config(void)
{
  uint32_t err_code;

  nrf_drv_twi_config_t const twi_config = {
      .scl	  	  = TWIM0_SCL,
      .sda	  	  = TWIM0_SDA,
      .frequency	  = NRF_DRV_TWI_FREQ_100K,
      .interrupt_priority = APP_IRQ_PRIORITY_LOWEST,
      .clear_bus_init     = false
  };

  err_code = nrf_drv_twi_init(&m_twi, &twi_config, twi_handler, NULL);
  APP_ERROR_CHECK(err_code);

  nrf_drv_twi_enable(&m_twi);
}

void log_init(void)
{
    ret_code_t err_code;

    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

int main(void){

  //ret_code_t err_code;

  log_init();
  bsp_board_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS);

  twi_config();

  /* Toggle LEDs. */
  //bool on_off = false;
  //bool flag = false;
      while (true)
      {
	  if(bsp_board_button_state_get(0)) bsp_board_led_invert(0);
	  if(bsp_board_button_state_get(1)) bsp_board_led_invert(1);

      }

}
