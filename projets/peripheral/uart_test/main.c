/**
 * Copyright (c) 2009 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
* @brief Example template project.
* @defgroup nrf_templates_example Example Template
*
*/

#include <stdio.h>
#include <stdlib.h>
#include "boards.h"
#include "nrf_delay.h"
#include "app_error.h"
#include "app_util_platform.h"
#include "nrf_drv_twi.h"
#include "nrf_gpio.h"

#include "app_uart.h"
#include "nrf_uart.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* TWI instance ID. */
#if TWI0_ENABLED
#define TWI_INSTANCE_ID     0
#elif TWI1_ENABLED
#define TWI_INSTANCE_ID     1
#endif

#define UART_RX_BUF_SIZE	256
#define UART_TX_BUF_SIZE	256


/* Indicates if operation on TWI has ended. */
static volatile bool m_xfer_done = false;

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);
//static const nrf_twi_mngr_t m_twi_mngr = NRF_

/* Buffer for samples read from temperature sensor. */
//static uint8_t m_sample;

/**
 * @brief TWI events handler.
 */
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
        	NRF_LOG_INFO("Test.");
            }
            m_xfer_done = true;
            break;
        default:
            break;
    }
}

// TWI with transaction manager init
static void twi_config(void)
{
  uint32_t err_code;

  nrf_drv_twi_config_t const twi_config = {
      .scl	  	  = TWIM0_SCL,
      .sda	  	  = TWIM0_SDA,
      .frequency	  = NRF_DRV_TWI_FREQ_100K,
      .interrupt_priority = APP_IRQ_PRIORITY_LOWEST,
      .clear_bus_init     = false
  };

  err_code = nrf_drv_twi_init(&m_twi, &twi_config, twi_handler, NULL);
  APP_ERROR_CHECK(err_code);

  nrf_drv_twi_enable(&m_twi);
}



void log_init(void)
{
    ret_code_t err_code;

    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

void gpio_init(void){

  /* Setting outputs */
  nrf_gpio_cfg_output(DP_PORT_SEL);
  nrf_gpio_cfg_output(IMP_CTRL0);
  nrf_gpio_cfg_output(IMP_CTRL1);
  nrf_gpio_cfg_output(OUT_SEL0);
  nrf_gpio_cfg_output(OUT_SEL1);
  nrf_gpio_cfg_output(OUT_SEL2);
  nrf_gpio_cfg_output(OUT_SEL3);
  nrf_gpio_cfg_output(OUT_SEL4);
  nrf_gpio_cfg_output(GAIN_FB);
  nrf_gpio_cfg_output(ANAL_ON);
  nrf_gpio_cfg_output(DP_SEL_P);
  nrf_gpio_cfg_output(DP_SEL_N);
  /* Setting inputs */
  nrf_gpio_cfg_input(BAT_STATUS, NRF_GPIO_PIN_NOPULL);
  /* Clearing outputs */
  nrf_gpio_pin_clear(DP_PORT_SEL);
  nrf_gpio_pin_clear(IMP_CTRL0);
  nrf_gpio_pin_clear(IMP_CTRL1);
  nrf_gpio_pin_clear(OUT_SEL0);
  nrf_gpio_pin_clear(OUT_SEL1);
  nrf_gpio_pin_clear(OUT_SEL2);
  nrf_gpio_pin_clear(OUT_SEL3);
  nrf_gpio_pin_clear(OUT_SEL4);
  nrf_gpio_pin_clear(GAIN_FB);
  nrf_gpio_pin_clear(ANAL_ON);
  nrf_gpio_pin_clear(DP_SEL_P);
  nrf_gpio_pin_clear(DP_SEL_N);

}


void uart_error_handler(app_uart_evt_t * p_event){
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
      {
          APP_ERROR_HANDLER(p_event->data.error_communication);
      }
      else if (p_event->evt_type == APP_UART_FIFO_ERROR)
      {
          APP_ERROR_HANDLER(p_event->data.error_code);
      }
}



int main(void){

  ret_code_t err_code;

  log_init();

  bsp_board_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS);

  gpio_init();

  //nrf_gpio_pin_set(ANAL_ON);

  twi_config();

  printf("Starto!");

  bsp_board_led_on(BSP_LED_1);

  NRF_LOG_INFO("Start!");
  NRF_LOG_FLUSH();

  // configuration param uart
  app_uart_comm_params_t comm_param = {
      RX_PIN_NUMBER,
      TX_PIN_NUMBER,
      0,
      0,
      APP_UART_FLOW_CONTROL_DISABLED,
      false,
      NRF_UART_BAUDRATE_115200
  };
  // safe init fifo uart
  APP_UART_FIFO_INIT(&comm_param,
		     UART_RX_BUF_SIZE,
		     UART_TX_BUF_SIZE,
		     uart_error_handler,
		     APP_IRQ_PRIORITY_THREAD ,
		     err_code);
  APP_ERROR_CHECK(err_code);
  while (true)
  {
      uint8_t cr;
      while (app_uart_get(&cr) != NRF_SUCCESS);
      bsp_board_led_invert(1);
      while (app_uart_put(cr) != NRF_SUCCESS);
  }

}
