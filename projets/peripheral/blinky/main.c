/*
 * main.c
 *
 *  Created on: 16 sept. 2019
 *      Author: Steve Mermet
 */

#include <stdio.h>
#include <stdlib.h>
#include "boards.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"



int main(void){

  /* Init LEDs*/
  bsp_board_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS);


  /* Toggle LEDs. */
  //bool on_off = false;
  //bool flag = false;
      while (true)
      {
	  if(bsp_board_button_state_get(0)) bsp_board_led_invert(0);
	  if(bsp_board_button_state_get(1)) bsp_board_led_invert(1);

      }

}
