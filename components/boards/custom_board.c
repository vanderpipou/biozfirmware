#include "custom_board.h"
#include "nrf_gpio.h"

void gpio_init(void){

  /* Config outputs */
  nrf_gpio_cfg_output(DP_PORT_SEL);
  nrf_gpio_cfg_output(IMP_CTRL0);
  nrf_gpio_cfg_output(IMP_CTRL1);
  nrf_gpio_cfg_output(OUT_SEL0);
  nrf_gpio_cfg_output(OUT_SEL1);
  nrf_gpio_cfg_output(OUT_SEL2);
  nrf_gpio_cfg_output(OUT_SEL3);
  nrf_gpio_cfg_output(OUT_SEL4);
  nrf_gpio_cfg_output(GAIN_FB);
  nrf_gpio_cfg_output(ANAL_ON);
  nrf_gpio_cfg_output(DP_SEL_P);
  nrf_gpio_cfg_output(DP_SEL_N);
  /* Config inputs */
  nrf_gpio_cfg_input(BAT_STATUS, NRF_GPIO_PIN_NOPULL);
  nrf_gpio_cfg_input(DTR_PIN_NUMBER, NRF_GPIO_PIN_NOPULL);
  /* Setting/Clearing outputs */
  nrf_gpio_pin_clear(DP_PORT_SEL);
  nrf_gpio_pin_clear(IMP_CTRL0);
  nrf_gpio_pin_clear(IMP_CTRL1);
  nrf_gpio_pin_clear(OUT_SEL0);
  nrf_gpio_pin_clear(OUT_SEL1);
  nrf_gpio_pin_clear(OUT_SEL2);
  nrf_gpio_pin_clear(OUT_SEL3);
  nrf_gpio_pin_clear(OUT_SEL4);
  nrf_gpio_pin_clear(GAIN_FB);
  nrf_gpio_pin_clear(ANAL_ON);
  nrf_gpio_pin_set(DP_SEL_P);
  nrf_gpio_pin_clear(DP_SEL_N);

}

void set_output(uint8_t output){

  nrf_gpio_pin_write(OUT_SEL0, (output & (1<<0)));
  nrf_gpio_pin_write(OUT_SEL1, (output & (1<<1)));
  nrf_gpio_pin_write(OUT_SEL2, (output & (1<<2)));
  nrf_gpio_pin_write(OUT_SEL3, (output & (1<<3)));
  nrf_gpio_pin_write(OUT_SEL4, (output & (1<<4)));
}

void set_hp_freq(uint8_t set_freq){
  nrf_gpio_pin_write(IMP_CTRL0, (set_freq & (1<<0)));
  nrf_gpio_pin_write(IMP_CTRL1, (set_freq & (1<<1)));
}

void set_iv_gain(uint8_t iv_gain){
  nrf_gpio_pin_write(GAIN_FB, iv_gain & 0x01);
}
