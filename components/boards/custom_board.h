/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"
#include "app_uart.h"

// LEDs definitions for PCA10040
#define LEDS_NUMBER    2

#define LED_1          30
#define LED_2          31

#define LEDS_ACTIVE_STATE 0

#define LEDS_INV_MASK  LEDS_MASK

#define LEDS_LIST { LED_1, LED_2 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2

#define BUTTONS_NUMBER 2

#define BUTTON_START   28
#define BUTTON_1       28
#define BUTTON_2       29
#define BUTTON_STOP    29
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_ACTIVE_STATE 0

#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2

#define RX_PIN_NUMBER  14
#define TX_PIN_NUMBER  13
#define RTS_PIN_NUMBER  0
#define CTS_PIN_NUMBER  0
#define DTR_PIN_NUMBER 15
#define HWFC           APP_UART_FLOW_CONTROL_DISABLED

#define SPIM0_SCK_PIN   17  // SPI clock GPIO pin number.
#define SPIM0_MOSI_PIN  19  // SPI Master Out Slave In GPIO pin number.
#define SPIM0_MISO_PIN  16  // SPI Master In Slave Out GPIO pin number.
#define SPIM0_CS_PIN    20  // SPI Chip Select GPIO pin number.

#define TWIM0_SDA	11  // I2C Data pin number
#define TWIM0_SCL	12  // I2C Clock pin number

#define DP_PORT_SEL	2   // Disconnect Impedance to measure DDP

#define IMP_CTRL0	3   // Lowpass frequecy Select
#define	IMP_CTRL1	4   // 00: 225.75[KHz]
			    // 01: 22.58[kHz]
			    // 10: 2.26[kHz]
			    // 11: 225.75[Hz]

#define OUT_SEL0	5   // Well Selection
#define OUT_SEL1	6   // Well Selection
#define OUT_SEL2	7   // Well Selection
#define OUT_SEL3	8   // Well Selection
#define OUT_SEL4	9   // Well Selection

#define GAIN_FB		10  // Gain I-V converter, 0: 1k, 1: 10k

#define ANAL_ON		22  // Analog circuit on/off

#define nAL_CC		23  // LTC2941 Alarm Output or Charcge complete input

#define BAT_STATUS	24  // Charging/Charged Battery Status

#define DP_SEL_P	26  // DDP pin connect positive
#define DP_SEL_N	25  // DDP pin connect negative

#define ADDR_AD5933	0b0001101 // Impedance & Network Analyzer
#define ADDR_LTC6904	0b0010110 // Oscillator
#define ADDR_LTC2481	0b0100100 // Differential ADC
#define ADDR_LTC2941	0b1100100 // Battery Gauge

#define HP_FREQ_225KHZ			0
#define HP_FREQ_22K5HZ			1
#define HP_FREQ_2K25HZ			2
#define HP_FREQ_225HZ			3

void gpio_init(void);
void set_output(uint8_t);
void set_hp_freq(uint8_t);
void set_iv_gain(uint8_t);

#ifdef __cplusplus
}
#endif

#endif // CUSTOM_BOARD_H
