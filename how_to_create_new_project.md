# How to create a new project for TEER
1. Go in **_projets/peripheral_**, duplicate _template_project_ and rename it how you want. You can duplicate it in another folder than _peripheral_ but it must be in at same level than _template_project_ inside **_projets_** (in other words, the project folder must be located in _projets/<some_folder>_).
2. Go in **_<your_project_folder>/board_custom/s132/armgcc_** and modify the name of the project in the Makefile.
## If you use Eclipse
3. **File>Import...** and select **C/C++>Existing Code as Makefile Project**
4. Name it how you want it.
5. Click **Browse...** and go in all the way into **_armgcc_** in your project folder, then click **Select**
6. Select ARM Cross GCC and **Finish**
7. Right click on your project in the Project Explorer and select Properties
8. Under **C/C++ Build**, uncheck **Use default build command** and replace the **Build command** by ```make VERBOSE=1```. You can add ```-jN``` where ```N``` is the number of core you want to use simultaneously or just ```-j```
9. Under **C/C++ General>Preprocessor Includes Paths, Macros etc.**, select **CDT GCC Build Output Parser** and fill **Compiler command pattern** with ```(.*gcc)|(.*[gc]\+\+)```
10. Select **CDT ARM Cross GCC Built-in Compiler Settings** and fill **Command to get compiler specs** with ```arm-none-eabi-gcc ${FLAGS} ${cross_toolchain_flags} -E -P -v -dD "${INPUTS}"```
11. Now you should be able to build the project without errors and debug it.

### Alternatively
If you have already opened a project in eclipse, you can import settings directly from that project in **Manage configuration...**. You'll have to change the bouild location however to your new project.

## If you use only GCC
3. You can build this with ```make``` and flash with ```make flash```

# Troubleshooting
Don't forget to flash the soft device first. To do so: go in the armgcc folder and use ```make flash_softdevice```.

If you use uECC (micro-ECC), you ned to build it first.

If some libraries are missing although they're included, you need to check in the makefile if their path are included.

Don't forget to enable and configure the libraries, drivers, etc... in the sdk_config.h. You can use the sdk config tool with the command ```make sdk_config```.